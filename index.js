/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
function personsInfo(){
	let fullName = prompt("Enter your full name: ");
	let age = prompt("Enter your age: ");
	let location = prompt("Enter your location: ");
	alert("thank you for giving your data");

	console.log("Wecome to this page, " + fullName);
	console.log("You are now " + age + " years old");
	console.log("You are currently at " + location);
}

personsInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

function bandsFavorite(){
	console.log("1. The Beatles");
	console.log("2. Metallica");
	console.log("3. The Eagles");
	console.log("4. One Direction");
	console.log("5. EraserHeads");

}

bandsFavorite();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
function faveMovies(){
	let firstMovie = ("1. Wrong Turn 1");
	function nestedRotten(){
		let nestedRotten = "Rotten Tomatoes Rating: 50%";
		console.log(firstMovie);
		console.log(nestedRotten);
	}
	nestedRotten();

	let secondMovie = "2. Wrong Turn 2";
	function nestedRotten2(){
		let nestedRotten2 = "Rotten Tomatoes Rating: 30%";
		console.log(secondMovie);
		console.log(nestedRotten2);
	}
	nestedRotten2();

	let thirdMovie = "3. Wrong Turn 3";
	function nestedRotten3(){
		let nestedRotten3 = "Rotten Tomatoes Rating: 90%";
		console.log(thirdMovie);
		console.log(nestedRotten3);
	}
	nestedRotten3();

	let fourthMovie = "4. saw 1";
	function nestedRotten4(){
		let nestedRotten4 = "Rotten Tomatoes Rating: 80%";
		console.log(fourthMovie);
		console.log(nestedRotten4);
	}
	nestedRotten4();

	let fifthMovie = "5. cinderella";
	function nestedRotten5(){
		let nestedRotten5 = "Rotten Tomatoes Rating: 40%";
		console.log(fifthMovie);
		console.log(nestedRotten5);			
	}
	nestedRotten5();
}	
faveMovies();


/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
}

printFriends();